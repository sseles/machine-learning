﻿using System;
using MathLib.SignalProcessing;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathLib.UnitTest.ComplexTypes
{
    [TestClass]
    public class Convolution1DTests
    {
        [TestMethod]
        public void CreateConvolution1D()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0 });

            Convolution1D convolution = new Convolution1D(10, filter);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FilterException()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0, 0 });

            Convolution1D convolution = new Convolution1D(10, filter);
        }

        [TestMethod]
        public void Filter3()
        {
            Vector<double> filter = new DenseVector(new double[] { 1, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter3Stride2()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter, 2);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 1);
            }
        }

        [TestMethod]
        public void Filter3Stride3()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter, 3);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 1);
            }
        }

        [TestMethod]
        public void Filter3Stride5()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter, 3);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 1);
            }
        }

        [TestMethod]
        public void Filter5()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter5Stride2()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter, 2);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter5Stride3()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter, 3);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter5Stride5()
        {
            Vector<double> filter = new DenseVector(new double[] { 0, 1, 0, 1, 0 });
            Vector<double> input = new DenseVector(new double[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

            Convolution1D convolution = new Convolution1D(10, filter, 5);

            Vector<double> result = convolution.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 2);
            }
        }
    }
}
