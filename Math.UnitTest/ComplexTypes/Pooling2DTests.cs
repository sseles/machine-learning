﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using MathLib.SignalProcessing;
using MathLib.SignalProcessing.Extras;
// ReSharper disable All

namespace MathLib.UnitTest.ComplexTypes
{
    [TestClass]
    public class Pooling2DTests
    {
        [TestMethod]
        public void CreatePooling2D()
        {
            Pooling2D max = new Pooling2D(10, 2, PoolingType.MaxPooling);
            Pooling2D max5_4 = new Pooling2D(5, 4, 2, PoolingType.MaxPooling);
            Pooling2D average = new Pooling2D(10, 3, PoolingType.Average);
            Pooling2D average7_5 = new Pooling2D(7, 5, 3, PoolingType.Average);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PoolingException()
        {
            Pooling2D max = new Pooling2D(10, 0, PoolingType.MaxPooling);
        }

        [DataTestMethod]
        [DataRow(4)]
        [DataRow(5)]
        [ExpectedException(typeof(ArgumentException))]
        public void PoolingException(int pooling)
        {
            Pooling2D max = new Pooling2D(10, pooling, PoolingType.MaxPooling);
        }

        [DataTestMethod]
        [DataRow(2)]
        [DataRow(3)]
        public void MaxPoolingSize(int pooling)
        {
            double[] array = new double[25];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(5, 5, array);

            Pooling2D max = new Pooling2D(5, pooling, PoolingType.MaxPooling);

            Matrix<double> result = max.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 1);
            }
        }

        [DataTestMethod]
        [DataRow(2)]
        [DataRow(3)]
        public void MaxPoolingSizeMatrix7_8(int pooling)
        {
            double[] array = new double[56];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(7, 8, array);

            Pooling2D max = new Pooling2D(input.RowCount, input.ColumnCount, pooling, PoolingType.MaxPooling);

            Matrix<double> result = max.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 1);
            }
        }
    }
}
