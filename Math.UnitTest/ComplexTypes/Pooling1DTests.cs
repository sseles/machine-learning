﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using MathLib.SignalProcessing;
using MathLib.SignalProcessing.Extras;
// ReSharper disable All

namespace MathLib.UnitTest.ComplexTypes
{
    [TestClass]
    public class Pooling1DTests
    {
        [TestMethod]
        public void CreatePooling1D()
        {
            Pooling1D max = new Pooling1D(10, 2, PoolingType.MaxPooling);
            Pooling1D average = new Pooling1D(10, 3, PoolingType.Average);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PoolingException()
        {
            Pooling1D max = new Pooling1D(10, 0, PoolingType.MaxPooling);
        }

        [DataTestMethod]
        [DataRow(4)]
        [DataRow(5)]
        [ExpectedException(typeof(ArgumentException))]
        public void PoolingException(int pooling)
        {
            Pooling1D max = new Pooling1D(10, pooling, PoolingType.MaxPooling);
        }

        [DataTestMethod]
        [DataRow(2, 5)]
        [DataRow(3, 4)]
        public void MaxPoolingSize(int pooling, int length)
        {
            Vector<double> input = new DenseVector(new double[] { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 });

            Pooling1D max = new Pooling1D(input.Count, pooling, PoolingType.MaxPooling);

            Vector<double> result = max.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 2);
            }

            Assert.IsTrue(result.Count == length);
        }

        [DataTestMethod]
        [DataRow(2, 5)]
        [DataRow(3, 5)]
        public void MaxPoolingSizeArray11(int pooling, int length)
        {
            Vector<double> input = new DenseVector(new double[] { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1 });

            Pooling1D max = new Pooling1D(input.Count, pooling, PoolingType.MaxPooling);

            Vector<double> result = max.Calculate(input);

            foreach (double value in result)
            {
                Assert.IsTrue(value == 2);
            }

            Assert.IsTrue(result.Count == length);
        }
    }
}
