﻿using MathLib.SignalProcessing;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathLib.UnitTest.ComplexTypes
{
    [TestClass]
    public class Convolution2DTests
    {
        [TestMethod]
        public void CreateConvolution2D()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            Convolution2D convolution = new Convolution2D(10, filter);
        }

        [TestMethod]
        public void Filter3()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[25];
            for (int i = 0; i < 25; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(5, 5, array);

            Convolution2D convolution = new Convolution2D(5, filter);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter3Stride2()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[25];
            for (int i = 0; i < 25; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(5, 5, array);

            Convolution2D convolution = new Convolution2D(5, filter,2);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter3Stride3()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[25];
            for (int i = 0; i < 25; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(5, 5, array);

            Convolution2D convolution = new Convolution2D(5, filter, 3);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter3Stride5()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[25];
            for (int i = 0; i < 25; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(5, 5, array);

            Convolution2D convolution = new Convolution2D(5, filter, 5);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter5()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[100];
            for (int i = 0; i < 100; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(10, 10, array);

            Convolution2D convolution = new Convolution2D(10, filter);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter5Stride2()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[100];
            for (int i = 0; i < 100; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(10, 10, array);

            Convolution2D convolution = new Convolution2D(10, filter,2);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter5Stride3()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[100];
            for (int i = 0; i < 100; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(10, 10, array);

            Convolution2D convolution = new Convolution2D(10, filter, 3);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Filter5Stride5()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[100];
            for (int i = 0; i < 100; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(10, 10, array);

            Convolution2D convolution = new Convolution2D(10, filter, 5);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }

        [TestMethod]
        public void Matrix5x4()
        {
            Matrix<double> filter = new DenseMatrix(3, 3, new double[] { 0, 1, 0, 1, 0, 0, 0, 0, 0 });

            double[] array = new double[20];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 1;
            }
            Matrix<double> input = new DenseMatrix(5, 4, array);

            Convolution2D convolution = new Convolution2D(5,4, filter, 5);

            Matrix<double> result = convolution.Calculate(input);

            foreach (double value in result.ToArray())
            {
                Assert.IsTrue(value == 2);
            }
        }
    }
}
