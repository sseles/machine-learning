﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MathLib.SignalProcessing
{
    public class Convolution2D
    {
        public Matrix<double> Filter { get; }

        private readonly int _skip;
        private readonly int _inputRowEnd;
        private readonly int _inputColumnEnd;
        private readonly int _stride;
        private readonly int _outputRowSize;
        private readonly int _outputColumnSize;

        private Convolution2D(Matrix<double> filter, int stride)
        {
            if (!((filter.RowCount == 3 && filter.ColumnCount == 3) || (filter.RowCount == 5 && filter.ColumnCount == 5))) throw new ArgumentException("Filter size must be 3x3 or 5x5");
            
            if (stride < 0) throw new ArgumentException("Stride must be larger than 1");

            _skip = filter.RowCount / 2;

            _stride = stride;

            Filter = filter;
        }

        /// <summary>
        /// Used when Input Matrix is NxN size
        /// </summary>
        /// <param name="inputLength">Input Matrix length</param>
        /// <param name="filter">Convolution filter (size 3x3 or 5x5)</param>
        /// <param name="stride">Skip number, default 1</param>
        public Convolution2D(int inputLength, Matrix<double> filter, int stride = 1) : this(filter, stride)
        {
            _inputRowEnd = inputLength - _skip;

            _inputColumnEnd = _inputRowEnd;

            _outputRowSize = (inputLength - filter.RowCount) / stride + 1;

            _outputColumnSize = _outputRowSize;
        }

        /// <summary>
        /// Used when Input Matrix is NxM size
        /// </summary>
        /// <param name="rowLength">Row Size</param>
        /// <param name="columnLength">Column Size</param>
        /// <param name="filter">Convolution filter (size 3x3 or 5x5)</param>
        /// <param name="stride">Skip number, default 1</param>
        public Convolution2D(int rowLength, int columnLength, Matrix<double> filter, int stride = 1) : this(filter, stride)
        {
            _inputRowEnd = rowLength - _skip;

            _inputColumnEnd = columnLength - _skip;

            _outputRowSize = (rowLength - filter.RowCount) / stride + 1;

            _outputColumnSize = (columnLength - filter.RowCount) / stride + 1;
        }

        public Matrix<double> Calculate(Matrix<double> input)
        {
            Matrix<double> result = new DenseMatrix(_outputRowSize, _outputColumnSize);

            for (int y = _skip, columnIndex = 0; y < _inputColumnEnd; y += _stride, columnIndex++)
            {
                for (int x = _skip, rowIndex = 0; x < _inputRowEnd; x += _stride, rowIndex++)
                {
                    for (int dy = -_skip; dy <= _skip; dy++)
                    {
                        for (int dx = -_skip; dx <= _skip; dx++)
                        {
                            result[columnIndex, rowIndex] += input[y + dy, x + dx] * Filter[_skip + dy, _skip + dx];
                        }
                    }
                }
            }

            return result;
        }
    }
}