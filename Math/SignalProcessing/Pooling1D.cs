﻿using System;
using MathLib.SignalProcessing.Extras;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MathLib.SignalProcessing
{
    public class Pooling1D
    {
        private const int SKIP = 1;
        private const int STRIDE = 2;

        private readonly Func<double[], double> _poolingFunction;

        private readonly int _inputEnd;
        private readonly int _poolingSize;
        private readonly int _outputSize;

        public Pooling1D(int inputLength, int poolingSize, PoolingType type)
        {
            if (!(poolingSize == 2 || poolingSize == 3))
            {
                throw new ArgumentException("Pooling size must be 2 or 3");
            }
            _poolingSize = poolingSize;

            if (poolingSize == 2)
            {
                _inputEnd = inputLength;
            }
            else
            {
                _inputEnd = inputLength - SKIP;
            }

            _outputSize = (inputLength - poolingSize) / STRIDE + 1;

            _poolingFunction = PoolingFunction.GetFunction(type);
        }

        public Vector<double> Calculate(Vector<double> input)
        {
            Vector<double> result = new DenseVector(_outputSize);

            for (int i = SKIP, counter = 0; i < _inputEnd; i += STRIDE, counter++)
            {
                double[] array = new double[_poolingSize];
                for (int j = 0, k = -SKIP; j < _poolingSize; j++, k++)
                {
                    array[j] = input[i + k];
                }

                result[counter] = _poolingFunction(array);
            }

            return result;
        }
    }
}
