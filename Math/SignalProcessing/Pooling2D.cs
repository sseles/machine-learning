﻿using System;
using MathLib.SignalProcessing.Extras;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MathLib.SignalProcessing
{
    public class Pooling2D
    {
        private const int SKIP = 1;
        private const int STRIDE = 2;

        private readonly Func<double[], double> _poolingFunction;

        private readonly int _skipEnd;
        private readonly int _inputRowEnd;
        private readonly int _inputColumnEnd;
        private readonly int _poolingSize;
        private readonly int _outputRowSize;
        private readonly int _outputColumnSize;

        private Pooling2D(int poolingSize, PoolingType type)
        {
            if (!(poolingSize == 2 || poolingSize == 3))
            {
                throw new ArgumentException("Pooling size must be 2 or 3");
            }

            _poolingSize = poolingSize * poolingSize;

            _poolingFunction = PoolingFunction.GetFunction(type);
        }

        /// <summary>
        /// When Matrix is NxN input size is N
        /// </summary>
        /// <param name="inputLength">N size</param>
        /// <param name="poolingSize">Pooling size 2 or 3</param>
        /// <param name="type">Average or Max Pooling</param>
        public Pooling2D(int inputLength, int poolingSize, PoolingType type) : this(poolingSize, type)
        {
            if (poolingSize == 2)
            {
                _inputRowEnd = inputLength;

                _inputColumnEnd = _inputRowEnd;

                _skipEnd = 1;
            }
            else
            {
                _inputRowEnd = inputLength - SKIP;

                _inputColumnEnd = _inputRowEnd;

                _skipEnd = 2;
            }

            _outputRowSize = (inputLength - poolingSize) / STRIDE + 1;

            _outputColumnSize = _outputRowSize;
        }

        public Pooling2D(int rowLength, int columnLength, int poolingSize, PoolingType type) : this(poolingSize, type)
        {
            if (poolingSize == 2)
            {
                _inputRowEnd = rowLength;

                _inputColumnEnd = columnLength;
            }
            else
            {
                _inputRowEnd = rowLength - SKIP;

                _inputColumnEnd = columnLength - SKIP;
            }

            _outputRowSize = (rowLength - poolingSize) / STRIDE + 1;

            _outputColumnSize = (columnLength - poolingSize) / STRIDE + 1;
        }

        public Matrix<double> Calculate(Matrix<double> input)
        {
            Matrix<double> result = new DenseMatrix(_outputRowSize, _outputColumnSize);

            for (int y = SKIP, columnIndex = 0; y < _inputColumnEnd; y += STRIDE, columnIndex++)
            {
                for (int x = SKIP, rowIndex = 0; x < _inputRowEnd; x += STRIDE, rowIndex++)
                {
                    int counter = 0;
                    double[] array = new double[_poolingSize];

                    for (int dy = -SKIP; dy < _skipEnd; dy++)
                    {
                        for (int dx = -SKIP; dx < _skipEnd; dx++)
                        {
                            array[counter++] = input[y + dy, x + dx];
                        }
                    }

                    result[columnIndex, rowIndex] = _poolingFunction(array);
                }
            }

            return result;
        }
    }
}
