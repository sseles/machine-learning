﻿using System;

namespace MathLib.SignalProcessing.Extras
{
    public enum PoolingType
    {
        Average,
        MaxPooling
    }

    public static class PoolingFunction
    {
        public static Func<double[], double> GetFunction(PoolingType type)
        {
            switch (type)
            {
                case PoolingType.MaxPooling:
                    return MaxPooling;
                default:
                    return AveragePooling;
            }
        }

        private static double MaxPooling(double[] array)
        {
            double result = 0;

            for (int i = 0; i < array.Length; i++)
            {
                result = Math.Max(result, array[i]);
            }

            return result;
        }

        private static double AveragePooling(double[] array)
        {
            double result = 0;

            for (int i = 0; i < array.Length; i++)
            {
                result += array[i];
            }

            return result / array.Length;
        }
    }
}
