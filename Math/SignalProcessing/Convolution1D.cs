﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace MathLib.SignalProcessing
{
    public class Convolution1D
    {
        public Vector<double> Filter { get; }

        private readonly int _inputStart;
        private readonly int _inputEnd;
        private readonly int _stride;
        private readonly int _outputSize;

        public Convolution1D(int inputLength, Vector<double> filter, int stride = 1)
        {
            if (!(filter.Count == 3 || filter.Count == 5)) throw new ArgumentException("Filter size must be 3 or 5");
            if (inputLength < filter.Count) throw new ArgumentException("Input length is too small");
            if (stride < 1) throw new ArgumentException("Stride must be larger than 0");

            _inputStart = filter.Count / 2;

            _inputEnd = inputLength - _inputStart;

            _outputSize = (inputLength - filter.Count) / stride + 1;

            _stride = stride;

            Filter = filter;
        }

        public Vector<double> Calculate(Vector<double> input)
        {
            Vector<double> result = new DenseVector(_outputSize);

            for (int i = _inputStart, counter = 0; i < _inputEnd; i += _stride, counter++)
            {
                for (int j = 0, k = -_inputStart; j < Filter.Count; j++, k++)
                {
                    result[counter] += input[i + k] * Filter[j];
                }
            }

            return result;
        }
    }
}
