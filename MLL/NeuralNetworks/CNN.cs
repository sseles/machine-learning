﻿using System;
using MathNet.Numerics.LinearAlgebra.Double;
using MLL.NeuralNetworks.Extras;

namespace MLL.NeuralNetworks
{
    /// <summary>
    /// Convolutional Neural Networks
    /// </summary>
    public class CNN : NeuralNetwork
    {
        private Func<double, double> _reluFunction;

        public CNN(int convolution, int pooling, params int[] layers) : base(layers)
        {
            _reluFunction = ActivationFunction.Rectifier;
        }

        //public override Matrix FeedForward(Matrix inputs)
        //{
        //    return base.FeedForward(inputs);
        //}
    }
}
