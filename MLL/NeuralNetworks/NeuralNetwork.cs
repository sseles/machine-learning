﻿using System;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MLL.NeuralNetworks.Extras;

namespace MLL.NeuralNetworks
{
    public class NeuralNetwork
    {
        public delegate double ActivationDelegate(double input);

        public double LearningRate
        {
            set => _learningRate = value;
        }

        private double _learningRate;
        private Func<double, double> _activationFunction;
        private Func<double, double> _derivativeFunction;

        protected readonly Matrix<double>[] _weightLayers;
        protected readonly Vector<double>[] _biasLayers;
        protected readonly int _layersCount;

        public NeuralNetwork(params int[] layers)
        {
            _learningRate = 0.1;
            _layersCount = layers.Length - 1;
            _weightLayers = new Matrix<double>[_layersCount];
            _biasLayers = new Vector<double>[_layersCount];

            for (int i = 0; i < _layersCount; i++)
            {
                _weightLayers[i] = Matrix<double>.Build.Random(layers[i + 1], layers[i], new Normal(0, 1));
                _biasLayers[i] = new DenseVector(layers[i + 1]);
            }

            SetActivationFunction(ActivationFunction.Sigmoid, ActivationFunction.SigmoidDer);
        }

        public virtual Vector<double> FeedForward(Vector<double> inputs)
        {
            Vector<double> layer = _weightLayers[0].Multiply(inputs);
            layer = layer.Add(_biasLayers[0]);
            layer = layer.Map(_activationFunction);

            for (int i = 1; i < _layersCount; i++)
            {
                layer = _weightLayers[i].Multiply(layer);
                layer = layer.Add(_biasLayers[i]);
                layer = layer.Map(_activationFunction);
            }

            return layer;
        }

        public virtual void Train(Vector<double> inputs, Vector<double> targets)
        {
            int current = _layersCount;

            Vector<double>[] layers = FeedForwardArrays(inputs); //Vector

            Vector<double> errors = targets.Subtract(layers[current]);//Vector

            Vector<double> gradient = layers[current].Map(_derivativeFunction);//Vectors

            gradient = gradient.PointwiseMultiply(errors);   //Vectors

            gradient = gradient.Multiply(_learningRate); //Vectors

            Matrix<double> delta = gradient.OuterProduct(layers[current - 1]);  //Matrix

            _weightLayers[current - 1] = _weightLayers[current - 1].Add(delta);   //Matrix
            _biasLayers[current - 1] = _biasLayers[current - 1].Add(gradient); //Vectors

            current--;
            while (current > 0)
            {
                errors = _weightLayers[current].Transpose().Multiply(errors);  //Vectors

                gradient = layers[current].Map(_derivativeFunction);  //Vectors
                gradient = gradient.PointwiseMultiply(errors);   //Vectors
                gradient = gradient.Multiply(_learningRate);    //Vectors

                delta = gradient.OuterProduct(layers[current - 1]);     //Matrix

                _weightLayers[current - 1] = _weightLayers[current - 1].Add(delta);  //Matrix
                _biasLayers[current - 1] = _biasLayers[current - 1].Add(gradient);   //Vectors

                current--;
            }
        }

        private Vector<double>[] FeedForwardArrays(Vector<double> inputs)
        {
            Vector<double>[] layers = new Vector<double>[_layersCount + 1];
            layers[0] = inputs;

            for (int i = 0; i < _layersCount; i++)
            {
                layers[i + 1] = _weightLayers[i].Multiply(layers[i]);
                layers[i + 1] = layers[i + 1].Add(_biasLayers[i]);
                layers[i + 1] = layers[i + 1].Map(_activationFunction);
            }

            return layers;
        }

        public void SetActivationFunction(Func<double, double> function, Func<double, double> derivative)
        {
            _activationFunction = function;

            _derivativeFunction = derivative;
        }
    }
}
