﻿using System;

namespace MLL.NeuralNetworks.Extras
{
    public static class ActivationFunction
    {
        public static double Sigmoid(double value)
        {
            return 1.0d / (1.0d + Math.Exp(-value));
        }

        /// <summary>
        /// Derivative Sigmoid
        /// </summary>
        /// <param name="value">Sigmoid Value</param>
        public static double SigmoidDer(double value)
        {
            return value * (1 - value);
        }

        /// <summary>
        /// Derivative Sigmoid
        /// </summary>
        /// <param name="value">Any value</param>
        public static double SigmoidDerFull(double value)
        {
            return Sigmoid(value) * (1 - Sigmoid(value));
        }

        public static double Rectifier(double value)
        {
            return Math.Max(value, 0);
        }
    }
}
