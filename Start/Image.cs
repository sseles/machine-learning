﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Start
{
    public class ImageConverter
    {
        public static BitmapSource CreateMonochrome(byte[] imageData, int width, int height)
        {
            BitmapSource image = BitmapSource.Create(width, height, 96, 96, System.Windows.Media.PixelFormats.Gray8, null, imageData, width);

            image.Freeze();

            return image;
        }
    }
}
