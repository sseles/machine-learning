﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Start
{
    public class Perceptron
    {
        private float lr = 0.1f;

        private readonly float[] _weights;

        public Perceptron(int n)
        {
            Random r = new Random();

            _weights = new float[n];

            for (int i = 0; i < n; i++)
            {
                _weights[i] = r.Next(-1, 1);
            }
        }

        public int Guess(float[] inputs)
        {
            float sum = 0;

            for (int i = 0; i < _weights.Length; i++)
            {
                sum += inputs[i] * _weights[i];
            }

            return Sign(sum);
        }

        public void Train(float[] inputs, int target)
        {
            int guess = Guess(inputs);
            int error = target - guess;

            for (int i = 0; i < _weights.Length; i++)
            {
                _weights[i] += error * inputs[i] * lr;
            }
        }

        public double GuessY(float x)
        {
            return -(_weights[2] / _weights[1]) - (_weights[0] / _weights[1]) * x;
        }

        private int Sign(float input)
        {
            if (input >= 0)
            {
                return 1;
            }

            return -1;
        }
    }
}
