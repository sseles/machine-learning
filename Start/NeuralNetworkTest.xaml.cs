﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MLL.NeuralNetworks;

namespace Start
{
    /// <summary>
    /// Interaction logic for NeuralNetworkTest.xaml
    /// </summary>
    public partial class NeuralNetworkTest : Window
    {
        private List<TestDataObject> testData = new List<TestDataObject>();

        Random r = new Random();

        NeuralNetwork greg = new NeuralNetwork(2, 4, 1);

        private int width;
        private int height;
        private byte[] imageArray;

        private void AddTestData()
        {
            testData.Add(new TestDataObject(new double[] { 0, 1 }, new double[] { 1 }));
            testData.Add(new TestDataObject(new double[] { 1, 0 }, new double[] { 1 }));
            testData.Add(new TestDataObject(new double[] { 0, 0 }, new double[] { 0 }));
            testData.Add(new TestDataObject(new double[] { 1, 1 }, new double[] { 0 }));
        }

        public NeuralNetworkTest()
        {
            InitializeComponent();

            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            width = (int)15;
            height = (int)15;

            var inputA = new double[2] { 1, 0 };
            var inputB = new double[2] { 0, 1 };
            var inputC = new double[2] { 0, 0 };
            var inputD = new double[2] { 1, 1 };

            AddTestData();
            greg.LearningRate = 0.1;

            imageArray = new byte[width * height];

            Picture.Source = CreateMonochrome(imageArray, width, height);
        }

        private class TestDataObject
        {
            public Vector<double> Inputs { get; set; }
            public Vector<double> Targets { get; set; }

            public TestDataObject(double[] inputs, double[] targets)
            {
                Inputs = new DenseVector(inputs);

                Targets = new DenseVector(targets); ;
            }
        }


        private int counter = 0;
        private void NeuralNetworkTest_OnKeyDown(object sender, KeyEventArgs e)
        {

            for (int i = 0; i < 10; i++)
            {
                int index = r.Next(0, 4);
                TestDataObject data = testData[index];
                greg.Train(data.Inputs, data.Targets);
                counter++;
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    //double[] inputs = new double[2] { (double)i / (double)width, (double)j / (double)height };

                    Vector<double> inputs = new DenseVector(2) { (double)i / (double)width, (double)j / (double)height };

                    double result = greg.FeedForward(inputs)[0];

                    imageArray[i + j * width] = (byte)((result) * 255);
                }
            }

            Picture.Source = CreateMonochrome(imageArray, width, height);
            LabelCounter.Content = "Counter " + counter;
        }

        public static BitmapSource CreateMonochrome(byte[] imageData, int width, int height)
        {
            BitmapSource image = BitmapSource.Create(width, height, 96, 96, System.Windows.Media.PixelFormats.Gray8, null, imageData, width);

            image.Freeze();

            return image;
        }

        public byte[] BufferFromImage(BitmapImage imageSource)
        {
            Stream stream = imageSource.StreamSource;
            byte[] buffer = null;

            if (stream != null && stream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    buffer = br.ReadBytes((Int32)stream.Length);
                }
            }

            return buffer;
        }
    }
}
