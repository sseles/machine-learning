﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MLL.NeuralNetworks;

namespace Start
{
    /// <summary>
    /// Interaction logic for StringRecognition.xaml
    /// </summary>
    public partial class StringRecognition : Window
    {
        StringValue[] points = new StringValue[100];

        Perceptron greg = new Perceptron(5);

        private int counter = 0;

        public StringRecognition()
        {
            InitializeComponent();

            StackPanel[] panels = new StackPanel[5];

            panels[0] = LabelPanel1;
            panels[1] = LabelPanel2;
            panels[2] = LabelPanel3;
            panels[3] = LabelPanel4;
            panels[4] = LabelPanel5;

            for (int i = 0, j = 0; i < 100; i++)
            {
                if (i % 20 == 0 && i != 0) j++;

                var str = RandomString(4);

                StringValue value = new StringValue(str);

                Label label = new Label { Content = str };


                panels[j].Children.Add(label);

                points[i] = value;

                value.Control = label;

                int guess = greg.Guess(value.Values);

                if (guess == 1) label.Background = Brushes.Green;
                else label.Background = Brushes.Red;

                Thread.Sleep(20);
            }
        }

        private void ColorLabels()
        {
            for (int i = 0; i < 100; i++)
            {
                Label label = points[counter].Control;

                int guess = greg.Guess(points[counter].Values);

                if (guess == 1) label.Background = Brushes.Green;
                else label.Background = Brushes.Red;
            }
        }

        private string RandomString(int length)
        {
            StringBuilder stringBuilder = new StringBuilder();
            Random random = new Random();

            char letter;

            for (int i = 0; i < length; i++)
            {
                double flt = random.NextDouble();
                int shift = Convert.ToInt32(System.Math.Floor(25 * flt));
                letter = Convert.ToChar(shift + 65);
                stringBuilder.Append(letter);
            }

            return stringBuilder.ToString();
        }

        private void StringRecognition_OnKeyDown(object sender, KeyEventArgs e)
        {
            greg.Train(points[counter].Values, points[counter].Label);

            ColorLabels();

            counter++;

            Counter.Content = counter;

            if (counter == points.Length - 1) counter = 0;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                if (points[i].Label == 1)
                {
                    points[i].Control.Foreground = Brushes.Blue;
                }
            }
        }

        private void StringRecognition_OnMouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
