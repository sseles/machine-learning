﻿using System;
using System.Windows.Shapes;

namespace Start
{
    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Bias { get; set; }
        public int Label { get; set; }

        public Ellipse Drawing { get; set; }

        public Point(int min, int max)
        {
            Random r = new Random();

            X = r.Next(min, max);

            Y = r.Next(min, max);

            Bias = 1;

            double line = Functions.Function(X);

            if (Y >= line) Label = 1;

            else Label = -1;
        }

        public float[] Array()
        {
            return new[] { X, Y, Bias };
        }
    }
}
