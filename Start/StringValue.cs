﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Start
{
    public class StringValue
    {
        public float[] Values { get; private set; }

        public int Label { get; set; }

        public Label Control { get; set; }

        public StringValue(string value)
        {
            Values = new float[value.Length + 1];

            for (int i = 0; i < value.Length; i++)
            {
                Values[i] = value[i];
            }

            Values[Values.Length - 1] = 1;

            if (Regex.IsMatch(value, @"(\w)\1"))
            {
                Label = 1;
            }
            else
            {
                Label = -1;
            }
        }
    }
}
