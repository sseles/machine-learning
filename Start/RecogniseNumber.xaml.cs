﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.Win32;
using MLL.NeuralNetworks;

namespace Start
{
    /// <summary>
    /// Interaction logic for RecogniseNumber.xaml
    /// </summary>
    public partial class RecogniseNumber : Window
    {
        NeuralNetwork greg = new NeuralNetwork(784, 200,20, 10);

        private List<ImageData> data;

        private const string IterationNumber = "Iteration Number - ";

        private Vector<double> guessImageArray;

        public RecogniseNumber()
        {
            InitializeComponent();

            ReadImages();
        }

        public void ReadImages()
        {
            string pathImages = "C:\\Users\\Home\\Desktop\\Test\\train-images.idx3-ubyte";
            string pathLabels = "C:\\Users\\Home\\Desktop\\Test\\train-labels.idx1-ubyte";

            data = ImageReader.ReadTestData(pathImages, pathLabels);

            guessImageArray = data[0].ImageArray;

            ImageXML.Source = data[0].ImageSource;
        }

        private async void ButtonStart_OnClick(object sender, RoutedEventArgs e)
        {
            running = true;
            await Task.Run(Iteration);
        }

        private void ButtonStop_OnClick(object sender, RoutedEventArgs e)
        {
            running = false;
        }

        private bool running;
        private int counter = 0;
        Random r = new Random();
        private void Iteration()
        {
            while (counter < data.Count)
            {
                //int current = r.Next(0, data.Count);
                if (running == false) break;

                Stopwatch s = new Stopwatch();
                s.Start();
                greg.Train(data[counter].ImageArray, data[counter].Labels);
                s.Stop();
                Console.WriteLine(s.ElapsedMilliseconds);
                s.Reset();
                counter++;
                Vector<double> result = greg.FeedForward(guessImageArray);

                Application.Current.Dispatcher.Invoke(
                    () =>
                    {
                        GuessChance(result);
                        LabelIteration.Content = IterationNumber + counter;
                    });
            }
        }

        private int guessNumber = 0;
        private void ButtonLoad_OnClick(object sender, RoutedEventArgs e)
        {
            running = false;

            guessImageArray = data[++guessNumber].ImageArray;

            ImageXML.Source = data[guessNumber].ImageSource;
            SaveClipboardImageToFile(@"C:\Users\Home\Desktop\Test\" + guessNumber + ".bmp", data[guessNumber].ImageSource);
            //data[guessNumber].ImageSource;
        }

        private void ButtonGuess_OnClick(object sender, RoutedEventArgs e)
        {
            running = false;

            Vector<double> result = greg.FeedForward(guessImageArray);

            GuessChance(result);
        }

        private void GuessChance(Vector<double> output)
        {
            Label0.Content = string.Format("Number 0 chance {0}%", (output[0] * 100).ToString("F"));
            Label1.Content = string.Format("Number 1 chance {0}%", (output[1] * 100).ToString("F"));
            Label2.Content = string.Format("Number 2 chance {0}%", (output[2] * 100).ToString("F"));
            Label3.Content = string.Format("Number 3 chance {0}%", (output[3] * 100).ToString("F"));
            Label4.Content = string.Format("Number 4 chance {0}%", (output[4] * 100).ToString("F"));
            Label5.Content = string.Format("Number 5 chance {0}%", (output[5] * 100).ToString("F"));
            Label6.Content = string.Format("Number 6 chance {0}%", (output[6] * 100).ToString("F"));
            Label7.Content = string.Format("Number 7 chance {0}%", (output[7] * 100).ToString("F"));
            Label8.Content = string.Format("Number 8 chance {0}%", (output[8] * 100).ToString("F"));
            Label9.Content = string.Format("Number 9 chance {0}%", (output[9] * 100).ToString("F"));
        }

        private void ButtonFromFile_OnClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                var imageSource = new BitmapImage(new Uri(openFileDialog.FileName));

                var buffer = new byte[784];

                var image = new FormatConvertedBitmap(imageSource, PixelFormats.Gray8, null, 0d);
                image.CopyPixels(buffer, 28, 0);

                ImageData data = new ImageData(buffer, 1);
                guessImageArray = data.ImageArray;
                ImageXML.Source = data.ImageSource;


            }
        }

        public byte[] ImageToByte(BitmapImage imageSource)
        {
            byte[] data;
            BmpBitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageSource));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }

        public static void SaveClipboardImageToFile(string filePath, BitmapSource imageSource)
        {
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                BitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(imageSource));
                encoder.Save(fileStream);
            }
        }

        public class ImageData
        {
            public Vector<double> ImageArray { get; set; }
            public int Label { get; set; }

            public BitmapSource ImageSource { get; set; }

            public Vector<double> Labels
            {
                get => _labels;
            }

            private readonly Vector<double> _labels;

            public ImageData(byte[] imageArray, byte target)
            {
                double[] array = new double[imageArray.Length];

                for (int i = 0; i < imageArray.Length; i++)
                {
                    array[i] = (double)imageArray[i] / 255d;
                }

                ImageArray = new DenseVector(array);


                Label = target;

                double[] label = new double[10];
                label[target] = 1;

                _labels = new DenseVector(label);

                ImageSource = ImageConverter.CreateMonochrome(imageArray, 28, 28);
            }
        }

        public static class ImageReader
        {
            public static List<ImageData> ReadTestData(string imagesPath, string labelsPath)
            {
                BinaryReader images = new BinaryReader(new FileStream(imagesPath, FileMode.Open));
                BinaryReader labels = new BinaryReader(new FileStream(labelsPath, FileMode.Open));

                List<ImageData> data = new List<ImageData>();

                int magic = GetInt32(images);
                int numberOfImages = GetInt32(images);
                int width = GetInt32(images);
                int height = GetInt32(images);

                int magicLabel = GetInt32(labels);
                int numberOfLabels = GetInt32(labels);

                for (int i = 0; i < numberOfImages; i++)
                {
                    byte[] bytes = images.ReadBytes(width * height);

                    data.Add(new ImageData(bytes, labels.ReadByte()));
                }

                return data;
            }

            private static int GetInt32(BinaryReader br)
            {
                byte[] bytes = br.ReadBytes(sizeof(Int32));
                if (BitConverter.IsLittleEndian) Array.Reverse(bytes);
                return BitConverter.ToInt32(bytes, 0);
            }
        }

    }
}
