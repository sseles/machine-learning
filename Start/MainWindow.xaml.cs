﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Start
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly Point[] _points = new Point[100];

        readonly Perceptron _p = new Perceptron(3);

        private Line _adjustmentLine;

        private int _counter = 0;

        public MainWindow()
        {
            InitializeComponent();

            Initialize();

            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        public void Initialize()
        {
            for (int i = 0; i < _points.Length; i++)
            {
                _points[i] = new Point(-490, 490);

                _points[i].Drawing = Circle(_points[i].X, _points[i].Y);

                int guess = _p.Guess(_points[i].Array());

                if (guess == _points[i].Label) _points[i].Drawing.Fill = Brushes.Green;

                Thread.Sleep(20);
            }

            Line(-500,500);
        }

        public Ellipse Circle(float x, float y)
        {
            Ellipse circle = new Ellipse()
            {
                Width = 10,
                Height = 10,
                StrokeThickness = 6,
                Fill = Brushes.Blue
            };

            MainCanvas.Children.Add(circle);

            circle.SetValue(Canvas.LeftProperty, ToScreenX(x));

            circle.SetValue(Canvas.TopProperty, ToScreenY(y));

            return circle;
        }

        public void Line(float x1, float x2)
        {
            double y1 = ToScreenX((float)Functions.Function(x1));
            double y2 = ToScreenX((float)Functions.Function(x2));

            x1 = (float)ToScreenX(x1);
            x2 = (float)ToScreenX(x2);

            Line line = new Line()
            {
                Stroke = Brushes.Black,
                X1 = x1,
                X2 = x2,
                Y1 = y1,
                Y2 = y2
            };

            _adjustmentLine = new Line()
            {
                Stroke = Brushes.DarkOrchid,
                X1 = x1,
                X2 = x2,
                Y1 = y1,
                Y2 = y2
            };

            MainCanvas.Children.Add(line);
            MainCanvas.Children.Add(_adjustmentLine);
        }

        private void MainCanvas_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.A)
            {
                _p.Train(_points[_counter].Array(), _points[_counter].Label);

                _adjustmentLine.Y1 = ToScreenY((float)_p.GuessY(-500));
                _adjustmentLine.Y2 = ToScreenY((float)_p.GuessY(500));

                ColorPoints();

                _counter++;

                LabelCounter.Content = _counter;
            }

            if (_counter == _points.Length - 1) _counter = 0;
        }

        private void ColorPoints()
        {
            for (int i = 0; i < _points.Length; i++)
            {
                Point point = _points[i];

                int guess = _p.Guess(point.Array());

                if (guess == 1)
                {
                    point.Drawing.Fill = Brushes.Green;
                }
                else
                {
                    point.Drawing.Fill = Brushes.Red;
                }
            }
        }

        private double ToScreenX(float cartesianX)
        {
            return (cartesianX + 1000 / 2);
        }

        private double ToScreenY(float cartesianY)
        {
            return (cartesianY + 1000 / 2);
        }
    }
}
